#
# this function read file and get the content, and the content will be set as a key of
# input the directory file
# {"filecontent"=>{:sample_file=>"A/content2", :count=>4}, "abcdefghijkl"=>{:sample_file=>"B/D/diff", :count=>1}} 
#
require "digest"

files = {}
Dir.glob('**/*').sort.each do|f|
  next if File.directory?(f)

  content = File.read(f)

  unless files.has_key?(content)
    files[content] = {sample_file: f, count: 1}
  else
    files[content][:count] += 1
  end
end

pp files

#
# this function using md5 and grouping with hexdigest 
# because if we read the content it would be too long because the file content
# input the directory file
# output : {md5File=>{:sample_file=>"A/content2", :count=>4}} 
#
require "digest"

files = {}
Dir.glob('**/*').sort.each do|f|
  next if File.directory?(f)
  digest = Digest::MD5.file(f).hexdigest
  unless files.has_key?(digest)
    files[digest] = {sample_file: f, count: 1}
  else
    files[digest][:count] += 1
  end
end

pp files